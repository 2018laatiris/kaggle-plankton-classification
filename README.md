# Kaggle Plankton Classification

Kaggle challenge on the classification of Plankton images. This project takes place within the deep learning course at CentraleSupelec.
Dataset contains 80 classes with heavy imbalances and around 900k training samples and 100k test samples.