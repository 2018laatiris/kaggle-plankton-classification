import sys
import subprocess
import preprocessing

args = sys.argv
if len(args) == 3 and args[1] == "train":
    preprocessing.create_data_csv(args[2], "train.csv")
    subprocess.call("python3 main_train.py --model_name Resnet50 --augment True --train_csv_path train.csv", shell=True)
elif len(args) == 4 and args[1] == "test":
    subprocess.call("python3 test_model --model_name Resnet50 --model_checkpoint_path " + args[2] + " --test_directory " + args[3], shell=True) 
else:
    print("args not in the correct format, check the kaggle challenge documentation for instructions")