import torch
from torch import nn
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader
from torchvision import transforms

from PIL import Image
import matplotlib.pyplot as plt

import os
from math import sqrt
from collections import Counter
import numpy as np
import pandas as pd
from sklearn import model_selection

from train import train_model
from models import prepare_model
import preprocessing
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--model_name',
                type=str,
                required = True,
                choices = ["Resnet18", "Resnet50", "Resnet50_fdf", "EfficientNet"],
                help='specifies the model to train',
                )

parser.add_argument('--top_logdir',
                type=str,
                default = "/usr/users/gpusdi1/gpusdi1_37/Documents/ChallengeDeep/logs",
                help='the top logging directory to store training info and model',
                )

parser.add_argument('--train_csv_path',
                type=str,
                default = "/usr/users/gpusdi1/gpusdi1_37/Documents/ChallengeDeep/train.csv",
                help='the path to the training csv file',
                )

parser.add_argument('--model_checkpoint_path',
                type=str,
                default = None,
                help="set if you want to load an existing model's parameters",
                )

parser.add_argument('--augment',
                type=bool,
                default = False,
                help='whether or not to augment training images',
                )

parser.add_argument('--lr',
                type=float,
                default = 3e-4,
                help='initial learning rate',
                )
parser.add_argument('--batch_size',
                type=int,
                default = 96,
                help='training and validation batch size',
                )
parser.add_argument('--eval_size',
                type=float,
                default = 0.2,
                help='relative size of the validation set',
                )
parser.add_argument('--num_epochs',
                type=int,
                default = 20,
                help='number of training epochs',
                )
parser.add_argument('--weighted_loss',
                type=bool,
                default = False,
                help='use a weighted cross entropy loss if set to True',
                )
parser.add_argument('--weighted_sampler',
                type=bool,
                default = False,
                help='use a weighted sampler in the data loader',
                )
parser.add_argument('--update_metric',
                type=str,
                choices = ["loss", "F1"],
                default = "loss",
                help='the metric to use in order to save the best performing model on the validation set',
                )
parser.add_argument('--randomize_split',
                type=bool,
                default = False,
                help="if True, will give a different train/validation split each training",
                )
args=parser.parse_args()

device = "cuda" if torch.cuda.is_available else "cpu"

logname = preprocessing.create_logname(args)
logdir = preprocessing.setup_logs(args.top_logdir, logname)
model = prepare_model(args.model_name, args.model_checkpoint_path)
model.to(device)

transformNormalize = transforms.Compose([preprocessing.SquarePad(), transforms.Resize((224,224)), transforms.ToTensor(), transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
transformAugment = transforms.Compose([transforms.RandomHorizontalFlip(0.3), transforms.RandomVerticalFlip(0.3), transforms.RandomRotation(degrees=20, fill=255), transformNormalize])

if args.augment:
    trainData = preprocessing.transformsDataset(args.train_csv_path, transforms=transformAugment)
else:
    trainData = preprocessing.transformsDataset(args.train_csv_path, transforms=transformNormalize)
evalData = preprocessing.transformsDataset(args.train_csv_path, transforms=transformNormalize)
print(trainData.csv_data.head())

if args.weighted_loss:
    train_classes = [label for label in pd.read_csv(args.train_csv_path)['label']]
    class_count = Counter(train_classes)
    class_weights = torch.Tensor([sqrt(len(train_classes)/c) for c in pd.Series(class_count).sort_index().values])
    class_weights = class_weights.to(device)
    loss_fn = nn.CrossEntropyLoss(class_weights)  
else:
    loss_fn = nn.CrossEntropyLoss()

if args.randomize_split:    
    train_split, eval_split = model_selection.train_test_split((pd.read_csv(args.train_csv_path)), test_size=args.eval_size)
else:
    train_split, eval_split = model_selection.train_test_split((pd.read_csv(args.train_csv_path)), test_size=args.eval_size, random_state=10)
trainData = torch.utils.data.Subset(trainData, train_split.index)
evalData = torch.utils.data.Subset(evalData, eval_split.index)

if args.weighted_sampler:
    train_classes = [int(label) for label in train_split['label']]
    class_count = Counter(train_classes)
    class_weights = torch.Tensor([sqrt(len(train_classes)/c) for c in pd.Series(class_count).sort_index().values])
    samples_weight = np.array([class_weights[t] for t in train_classes])
    samples_weight = torch.from_numpy(samples_weight)
    samples_weight = samples_weight.double()
    sampler = torch.utils.data.WeightedRandomSampler(samples_weight, len(samples_weight))
    train_loader = DataLoader(trainData, batch_size=args.batch_size, sampler=sampler)
else:
    train_loader = DataLoader(trainData, batch_size=args.batch_size, shuffle = True)
eval_loader = DataLoader(evalData, batch_size=args.batch_size, shuffle = True)
optimizer = torch.optim.Adam(model.parameters(), lr = args.lr)

tensorboard_writer   = SummaryWriter(log_dir = logdir)
best_model = train_model(model, optimizer, loss_fn, train_loader, eval_loader, device, args.lr, args.batch_size, args.num_epochs, tensorboard_writer, logdir, args.update_metric)

