import torchvision
import torch
from torch import nn

def prepare_model(model_name, load_path=None):
    """
    returns a certain model, will load a checkpoint if load_path is provided
    """
    if model_name == "Resnet18":
        model = torchvision.models.resnet18(pretrained=True)
        model.fc = nn.Linear(512, 86)
    elif model_name == "Resnet50":
        model = torchvision.models.resnet50(pretrained=True)
        model.fc = nn.Linear(2048, 86)
    elif model_name == "Resnet50_fdf":
        model = torchvision.models.resnet50(pretrained=True)
        model.fc = nn.Sequential(nn.Linear(2048, 2048), nn.ReLU(), nn.Dropout(0.3), nn.Linear(2048,86))
    elif model_name == "EfficientNet":
        model = torchvision.models.efficientnet_b3(pretrained=True)
        model.classifier = nn.Linear(1536, 86)
    else:
        print(f"incorrect model name: {model_name}")
    if load_path:
        print(f"loading {model_name} state from checkpoint: {load_path}")
        model.load_state_dict(torch.load(load_path))
    
    return model