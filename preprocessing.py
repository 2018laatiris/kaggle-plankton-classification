import pandas as pd
import os
import torch
from torch.utils.data import Dataset
from torchvision import transforms
from PIL import Image

def create_data_csv(root_dir, save_path=None):
    """
    creates a csv file with the following columns:
        path: the path of each image starting from the root_dir
        label: label of each image extracted from its location
    """
    data = pd.DataFrame({"path":pd.Series(dtype='str'), "label":pd.Series(dtype='int')})
    for class_dir in os.listdir(root_dir):
        files = os.listdir(os.path.join(root_dir,class_dir))
        label = None
        if class_dir.split("_")[0] == "000":
            label = 0
        else:
            label = int(class_dir.split("_")[0].lstrip('0'))
        data = data.append(pd.DataFrame({"path":[os.path.join(root_dir, class_dir, file) for file in files], "label":label}), ignore_index=True)
        if save_path:
            data = data.sample(frac=1).reset_index(drop=True)
            data.to_csv(save_path, index=False)
    return(data)
    
class transformsDataset(Dataset):
    def __init__(self, csv_path, transforms=None):
        self.root_dir = ""
        self.csv_data = pd.read_csv(csv_path)
        self.transforms = transforms
    def __len__(self):
        return(len(self.csv_data))
    def set_transform(self, transform):
        self.transforms = transform
    def __getitem__(self, index):
        image = Image.open(self.csv_data.loc[index]["path"]).convert('RGB')
        label = self.csv_data.loc[index]["label"]
        return(self.transforms(image), torch.tensor([label]).type(torch.LongTensor))

class testDataset(Dataset):
    def __init__(self, root_dir, csv_path, transforms=None):
        self.root_dir = ""
        self.csv_data = pd.read_csv(csv_path)
        self.transforms = transforms
    def __len__(self):
        return(len(self.csv_data))
    def __getitem__(self, index):
        image = Image.open(self.csv_data.loc[index]["path"]).convert('RGB')
        img_num = int(os.path.splitext(os.path.basename(self.csv_data.loc[index]["path"]))[0])
        return self.transforms(image), torch.tensor(img_num)

class SquarePad:
	def __call__(self, image):
		w, h = image.size
		max_wh = np.max([w, h])
		hp = int((max_wh - w) / 2)
		vp = int((max_wh - h) / 2)
		padding = (hp, vp, hp, vp)
		return transforms.Pad(padding, 255, 'constant')(image)

def generate_unique_logpath(logdir, raw_run_name):
    i = 0
    while(True):
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1

def setup_logs(top_logdir, logname):
    if not os.path.exists(top_logdir):
        os.mkdir(top_logdir)
    logdir = generate_unique_logpath(top_logdir, logname)
    print("Logging to {}".format(logdir))
    if not os.path.exists(logdir):
        os.mkdir(logdir)
    return logdir

def create_logname(args):
    """
    create a corresponding log directory name from the training arguments
    """
    logname = args.model_name
    if args.augment:
        logname += "_augmented"
    if args.weighted_loss:
        logname += "_weightedL"
    if args.weighted_sampler:
        logname += "_weightedS"
    return logname

