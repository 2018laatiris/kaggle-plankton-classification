import torch
from torch import nn
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader, Dataset
import torchvision
from torchvision import transforms
from PIL import Image
import matplotlib.pyplot as plt
import os
import pandas as pd
import tqdm
import copy
import models, preprocessing
import numpy as np
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--model_name',
                type=str,
                required = True,
                choices = ["Resnet18", "Resnet50", "Resnet50_fdf", "EfficientNet"],
                help='specifies the model to train',
                )
parser.add_argument('--model_checkpoint_path',
                type=str,
                default = None,
                help="set the folder name of the model to test",
                )
parser.add_argument('--test_directory',
                type=str,
                default = "/usr/users/gpusdi1/gpusdi1_37/Documents/ChallengeDeep/data/test/imgs",
                help="the path to the folder containing test images",
                )
args=parser.parse_args()
logdir = os.path.dirname(args.model_checkpoint_path)
device = "cuda" if torch.cuda.is_available else "cpu"
model = models.prepare_model("Resnet50", args.model_checkpoint_path)
model.to(device)
model.eval()

def create_test_data_csv(root_dir):
    """
    creates a csv file with the following columns:
        path: the path of each image starting from the root_dir
        label: label of each image extracted from its location
    """
    data = pd.DataFrame({"path":[]})
    files = os.listdir(root_dir)
    data = data.append(pd.DataFrame({"path":[os.path.join(root_dir, file) for file in files]}), ignore_index=True)
    return(data)
csv_test = create_test_data_csv(args.test_directory)
csv_test.to_csv("local_test.csv")
transformTest = transforms.Compose([preprocessing.SquarePad(), transforms.Resize((224,224)), transforms.ToTensor(), transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
transformedData = preprocessing.testDataset(args.test_directory, "local_test.csv", transforms=transformTest)
print(transformedData.csv_data.head())
batch_size = 256
test_loader = DataLoader(transformedData, batch_size=batch_size)
test_df = pd.DataFrame({"imgname": pd.Series(dtype='str'), "label": pd.Series(dtype='int')})
with torch.no_grad():
        for X,y in tqdm.tqdm(test_loader):
            X = X.to(device)
            pred = model(X)
            test_df = test_df.append(pd.DataFrame({"imgname" : [str(i)+".jpg" for i in y.squeeze().tolist()], "label":[int(i) for i in pred.argmax(axis=1).tolist()] }), ignore_index=True)
           
test_df.to_csv(os.path.join(logdir,"test_results2.csv"), index=False)
print("csv file is saved in the same folder as the model checkpoint")
