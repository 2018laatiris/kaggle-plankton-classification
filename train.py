import torch
import copy
import tqdm
import os
import time

def train_model(model, optimizer, loss_fn, train_loader, eval_loader, device, lr=3e-4, batch_size=96, epochs_num=20, tensorboard_writer=None, logdir=None, update_metric="loss"):
    start = time.time()
    best_eval_loss = 1000
    best_eval_F1 = 0
    best_model = copy.deepcopy(model.state_dict())
    print(f"training model for {epochs_num} epochs with {batch_size} batch size and {lr} initial learning rate, updating by {update_metric}")
    for epoch in range(epochs_num):
        model.train()
        running_loss = 0
        train_samples = 0
        correct_pred = 0
        batch_num = 0
        for X,y in tqdm.tqdm(train_loader):
            batch_num += 1
            #forward
            X = X.to(device)
            y = y.to(device)
            pred = model(X)
            loss = loss_fn(pred, y.squeeze())
            #backward
            optimizer.zero_grad()
            loss.backward()
            #update
            optimizer.step()
            running_loss += loss.item()*X.shape[0]
            train_samples += X.shape[0]
            correct_pred += (pred.argmax(axis=1) == y.squeeze()).sum()
            if batch_num %300 == 299:
                print(f"train at epoch {epoch} loss: {running_loss/train_samples}, accuracy: {correct_pred/train_samples}, step: {batch_num//300}")
        print(f"train at epoch {epoch} loss: {running_loss/train_samples}, accuracy: {correct_pred/train_samples}, step: finished epoch")
                #running_loss = 0
                #train_samples = 0
                #correct_pred = 0
        if tensorboard_writer:
            tensorboard_writer.add_scalar('metrics/train_loss', running_loss/train_samples, epoch)
            tensorboard_writer.add_scalar('metrics/train_acc',  correct_pred/train_samples, epoch)

        model.eval()
        correct_pred = 0
        eval_loss = 0
        eval_samples = 0
        num_labels = pred.shape[1]
        TP = [0 for i in range(num_labels)]
        FP = [0 for i in range(num_labels)]
        FN = [0 for i in range(num_labels)]
        with torch.no_grad():
            for X,y in tqdm.tqdm(eval_loader):
                X = X.to(device)
                y = y.to(device)
                pred = model(X)
                eval_loss += loss_fn(pred, y.squeeze()).item() * X.shape[0]
                correct_pred += (pred.argmax(axis=1) == y.squeeze()).sum()
                eval_samples += X.shape[0]
                for prediction, label in zip(pred.argmax(axis=1), y.squeeze()):
                    if prediction == label:
                        TP[prediction] += 1
                    else:
                        FP[prediction] += 1
                        FN[label] += 1
        eval_loss /= eval_samples
        accuracy = correct_pred / eval_samples
        
        precision_per_class = [0.001 if TP[i] + FP[i] == 0 else 0.0001 + TP[i]/(TP[i] + FP[i]) for i in range(len(TP))]
        recall_per_class = [0.001 if TP[i] + FN[i] == 0 else 0.0001 + TP[i]/(TP[i] + FN[i]) for i in range(len(TP))]
        F1_per_class = [2/(1/precision_per_class[i] + 1/recall_per_class[i]) for i in range(len(TP))]
        eval_macro_F1 = sum(F1_per_class)/len(F1_per_class)
        print(f"validation at epoch {epoch} loss: {eval_loss}, accuracy: {accuracy}, F1 {eval_macro_F1}")
        if tensorboard_writer:
            tensorboard_writer.add_scalar('metrics/val_loss', eval_loss, epoch)
            tensorboard_writer.add_scalar('metrics/val_acc',  accuracy, epoch)
            tensorboard_writer.add_scalar('metrics/val_F1',  eval_macro_F1, epoch)
            tensorboard_writer.add_scalar('compute time(minutes)',  (time.time()-start)/60, epoch)
        if update_metric == "loss":
            update_condition = (eval_loss < best_eval_loss)
        elif update_metric == "F1":
            update_condition = (eval_macro_F1 > best_eval_F1)
        if update_condition:
            best_model = copy.deepcopy(model.state_dict())
            if update_metric == "loss":
                best_eval_loss = eval_loss
            elif update_metric == "F1":
                best_eval_F1 = eval_macro_F1
            print(f"best copy of model updated at epoch {epoch} with a validation loss {eval_loss} and accuracy {accuracy} and F1 {eval_macro_F1}")
            if logdir:
                torch.save(model.state_dict(), os.path.join(logdir,"best_model.pt"))
            if tensorboard_writer:
                tensorboard_writer.add_scalar('best_model_update', epoch, epoch)
    return best_model
